# Ansible Playbooks for Commons Host

## Requirements

- [Ansible](https://github.com/ansible/ansible) to run the Playbooks.
- [VirtualBox](https://www.virtualbox.org) to run virtual machines.
- [Oracle VM VirtualBox Extension Pack](https://www.virtualbox.org/wiki/Downloads) to emulate an attached SSD.
- [Vagrant](https://www.vagrantup.com) to manage virtual machines.
- [GnuPG](https://gnupg.org) (macOS: [GPG Suite](https://gpgtools.org)) to access secrets in Ansible Vault.

## Installation

Install Python dependencies:

```
pip install -r requirements.txt
```

Install Ansible Galaxy roles:

```
ansible-galaxy install -r requirements.yml
```

For macOS: Install Vagrant dependencies:

```
brew install http://git.io/sshpass.rb
pip install passlib
```

## Inventories

There are separate inventory files for each type of server. This allows separate variables for each type of deployment and keeps the individual inventory files smaller.

[hosts/edge.production.yaml](./hosts/edge.production.yaml) - CDN edge PoPs (Odroid and cloud servers)

[hosts/edge.staging.yaml](./hosts/edge.staging.yaml) - development and testing (i.e. Vagrant/Virtualbox, local hardware servers, etc.)

[hosts/core.production.yaml](./hosts/core.production.yaml) - backend API production

[hosts/core.staging.yaml](./hosts/core.staging.yaml) - backend API staging

### GeoDNS PoP Naming Scheme

Hostnames in the production inventory follow a scheme based on the [closest major airport](https://www.closestairportto.com).

```
$COUNTRY-$CITY-$INDEX
```

`$COUNTRY` is the 2-letter country code defined by [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).

`$CITY` is the 3-letter city code defined by [IATA](https://en.wikipedia.org/wiki/IATA_airport_code), preferring [Metropolitan Area Airport Codes](https://wikitravel.org/en/Metropolitan_Area_Airport_Codes), if any.

`$INDEX` is an integer, starting from 1, incremented by 1 for each new edge server in the same city.

Examples:

- sg-sin-1.commons.host
- sg-sin-2.commons.host
- my-kul-1.commons.host
- lt-vno-1.commons.host

## Playbooks

`bootstrap.yaml` - Configures access permissions on a blank Odroid server.

`edge.yaml` - Deploy CDN edge PoP servers.

`core.yaml` - Deploy Core backend API servers.

`vagrant.yaml` - Provision Vagrant/Virtualbox with Odroid-like environment for development.

### Variables

#### SSD

Cache data can be set up on a specific storage device. Odroid HC1 servers use a dedicated SSD instead of a micro SD card to increase storage capacity, performance, and longevity.

Leave these settings undefined to use the default storage device for cache data.

| Name | Description | Default | Example |
|-|-|-|-|
| pop_ssd | SSD device location | *undefined* | /dev/sda |
| pop_ssd_mount | Data partition | *undefined* | /dev/sda1 |

#### Static IP Network Interface

Servers on private networks (i.e. NAT) are port forwarded and assigned a reserved IP by the local DHCP server. In case a router does not support reserved IP address assignments, as is the case with some old routers, it is possible to configure a static IP on the host itself.

Both `pop_network_address` and `pop_network_gateway` are required to set a static IPv4 or IPv6 address. By default DHCP is used, for both IPv4 and IPv6.

The `pop_network_apply` setting, which defaults to `true`, can be set to `false` to disable the network configuration altogether. This is useful with some containers or virtual machines, such as Linode VPS, where non-standard tools edit the OS network configuration files. Editing these settings could cause conflicts.

| Name | Description | Default | Example |
|-|-|-|-|
| pop_network_interface | The network device as reported by `ifconfig` | eth0 | eth1 |
| pop_network_address | Static IP address | | 192.168.1.123/24 |
| pop_network_gateway | Router IP address | | 192.168.1.1 |
| pop_network_apply | Set or skip network configuration | true | false |

#### Knot Resolver

See the Knot Resolver documentation on [scaling out](https://knot-resolver.readthedocs.io/en/stable/daemon.html#scaling-out) or [`kresd.systemd(7)`](https://www.mankier.com/7/kresd.systemd).

| Name | Description | Default | Example |
|-|-|-|-|
| knot_instances | Number of processes to run in parallel (maximum 8) | 1 | 4 |

#### SSH

Listening on port `22` with a public IPv4 address results in constant authentication probes by hackers attempting to log in to root and other accounts. To prevent this from cluttering the logs, `sshd` should listen on a different, non-standard port like `2222`. Servers behind a firewall (LAN router) can listen locally on port `22` and use port forwarding to map from `22` to `2222`. Servers directly exposed to the public Internet should rebind `sshd` to use port `2222`.

| Name | Description | Default | Example |
|-|-|-|-|
| pop_ssh_port | TCP port on which `sshd` listens | 22 | 2222 |

The external SSH port of the server (e.g. `2222`) should be added to the inventory as `ansible_ssh_port`. See the bootstrapping section below for details on specifying the initial SSH port, which should only need to be done once.

#### DDNS

Used on edge servers with a dynamic IP address, like many residential ISP connections. A system service polls the Core API [server DDNS endpoint](https://help.commons.host/core/endpoints/servers/#put-v2serversidddns) to update its DNS record with its current, public IP address. Example: `sg-sin-2.commons.host` -> `158.140.144.34`

| Name | Description | Default | Example |
|-|-|-|-|
| ddns | Boolean to enable (`true`) or disable (`false`) the DDNS client | false | true |

## Usage

### Adding a User to Access the Vault

1. Add their GPG public key to `/keys/gpg` with the file name: first name + `.` + last name. E.g. `jane.doe`
1. Add their SSH public key to `/keys/ssh` with the file name: first name + `.` + last name. E.g. `jane.doe`
1. Edit the script `re-encrypt-vault` to add their email and and file name.
1. Ask an existing vault user to run `re-encrypt-vault`. Git commit and push the updated `vault-password.gpg` file.

### Developing with Vagrant

Run `vagrant up` the first time to create a Ubuntu 16.04 server. This automatically *provisions* using Ansible.

The initial provisioning executes the `vagrant.yaml` Ansible playbook which sets up the root user like the Odroid Ubuntu distribution defaults.

Run `vagrant destroy` to delete the VM and start over.

Run `vagrant ssh` for shell access to the VM.

### Bootstrapping the Initial Server Configuration

Sets up the authentication and other security policies.

```
ansible-playbook -e "ansible_ssh_user=root ansible_ssh_pass=odroid ansible_ssh_port=22" -i hosts/edge.production.yaml -l hostname bootstrap.yaml
```

For the Vagrant staging environment, remove the previous SSH key for `[localhost]:2222` from `~/.ssh/known_hosts`, and run:

```
ansible-playbook -e "ansible_ssh_user=root ansible_ssh_pass=odroid ansible_ssh_port=2222" -i hosts/edge.staging.yaml -l virtualbox bootstrap.yaml
```

Replace `hostname` with the name assigned to the server in the inventory.

Replace `root` with the current superuser username.

Replace `odroid` with the current password for the superuser.

Alternatively, pass the `-k` option to interactively supply an override the SSH password for the `root` user. By default the password is `odroid`.

If a `sudo` password is required for the SSH user, pass it interactively via the `-K` option.

### Deploying 

To deploy to the core API box:

```
ansible-playbook -i hosts/core.production.yaml core.yaml
```

To deploy to all CDN edge pops:

```
ansible-playbook -i hosts/edge.production.yaml edge.yaml
```

Or, to deploy to a specific PoP:

```
ansible-playbook -i hosts/edge.production.yaml -l sg-sin-1 edge.yaml
```

Or, to deploy to PoPs in the same country:

```
ansible-playbook -i hosts/edge.production.yaml -l 'sg-*' edge.yaml
```

Limits by server type are also possible, namely `odroid` or `linode`.

```
ansible-playbook -i hosts/edge.production.yaml -l 'odroid' edge.yaml
```

To exclude hosts from selection, use negation selectors. Append multiple selectors using colon (`:`). For example match all hosts (`*`) *except* those in Singapore (`!sg-*`):

```
ansible-playbook -i hosts/edge.production.yaml -l '*:!sg-*' edge.yaml
```

### Ad-Hoc Server Commands

Should be avoided if possible; a playbook role is preferred. But running a command on one of more servers is sometimes convenient.

```
ansible -i ./hosts/edge.production.yaml --become -m service -a 'name=edge state=restarted' 'sg-sin-2'
```

### Monitoring Logs of the Systemd Edge Service

```
systemctl status edge
```

```
sudo journalctl -u edge -f -n 30
```

## ACME Certificate Renewal

Wildcard `*.commons.host` certificate is used as a fallback for all sites, if no domain-specific certificate is available. The `api.commons.host` certificate is used by the Core API. Automating these renewals is WIP. These instructions are recorded here as a personal reminder and reference guide.

### Renew wildcard certificate:

1. `~/.acme.sh/acme.sh --issue --keylength ec-256 --home /Users/sebdeckers/code/commonshost/pop/acme/store --domain '*.commons.host' --dns --yes-I-know-dns-manual-mode-enough-go-ahead-please`
1. Update the TXT record `_acme-challenge.commons.host` to the challenge value.
1. `~/.acme.sh/acme.sh --renew --keylength ec-256 --ecc --home /Users/sebdeckers/code/commonshost/pop/acme/store --domain '*.commons.host' --dns --yes-I-know-dns-manual-mode-enough-go-ahead-please`

See: https://github.com/Neilpang/acme.sh/wiki/dns-manual-mode

Run the `wildcard-cert-update.js` script to update the MongoDB `certificates` collection record for `*.commons.host` with the binary contents of:

- `/Users/sebdeckers/code/commonshost/pop/acme/store/*.commons.host_ecc/fullchain.cer`
- `/Users/sebdeckers/code/commonshost/pop/acme/store/*.commons.host_ecc/*.commons.host.key`

### Adding Certs to Ansible Vault

Copy `api.commons.host.cer`, `ca.cer`, and `api.commons.host.key` to `./roles/core/files`.

The private keys should be unchanged by a certificate renewal. They are not known to ACME.

## Colophon

Made with ❤️ by [Kenny Shen](https://machinesung.com) and [Sebastiaan Deckers](https://twitter.com/sebdeckers) in 🇸🇬 Singapore.
