const { MongoClient } = require('mongodb')
const { promises: { readFile } } = require('fs')

;(async function () {
  // Configure the connection string
  const mongodbUri = 'mongodb+srv://USERNAME:PASSWORD@ACCOUNT.mongodb.net'

  const mongodbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
  const client = await MongoClient.connect(mongodbUri, mongodbOptions)
  const db = client.db('http2live')
  const collection = db.collection('certificates')
  const domain = '*.commons.host'
  const query = { domain }
  const certPath = '/Users/sebdeckers/code/commonshost/pop/acme/store/*.commons.host_ecc/fullchain.cer'
  const keyPath = '/Users/sebdeckers/code/commonshost/pop/acme/store/*.commons.host_ecc/*.commons.host.key'
  const operation = {
    $set: {
      ca: [],
      cert: await readFile(certPath),
      domain,
      key: await readFile(keyPath),
      san: []
    },
    $currentDate: { modified: true }
  }
  console.log(query)
  await collection.updateOne(query, operation, { upsert: true })
  const document = await collection.findOne(query)
  console.log(document)
  await client.close()
})()
