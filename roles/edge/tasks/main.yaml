- name: Encrypted configuration variables
  include_vars:
    file: '../secrets/{{ node_environment }}.yaml'

- name: Install dependencies
  become: yes
  apt:
    pkg:
    - libsystemd-dev

- name: Install from NPM
  become: yes
  become_user: "{{ pop_user }}"
  shell: >
    source ~/.bash_profile &&
    npm install "@commonshost/edge@{{ edge_version }}"
    --no-save
    --prefix "{{ pop_home }}/node-v{{ node_version }}-edge-v{{ edge_version }}"
  args:
    executable: /bin/bash
    creates: "{{ pop_home }}/node-v{{ node_version }}-edge-v{{ edge_version }}/package-lock.json"

- name: Delete TLS wildcard credentials
  become: yes
  become_user: "{{ pop_user }}"
  file:
    path: "{{ pop_home }}/crypto"
    state: absent

- name: Copy placeholder
  become: yes
  become_user: "{{ pop_user }}"
  copy:
    src: "../files/{{ item }}"
    dest: "{{ pop_home }}/placeholder/"
    mode: 0644
  with_items:
  - host-not-found.html
  - gophermap

- name: Install configuration
  become: yes
  become_user: "{{ pop_user }}"
  template:
    src: ../templates/edge.conf.js
    dest: "{{ pop_home }}/edge.conf.js"
  notify:
  - Restart systemd service

- name: Install systemd configuration
  become: yes
  template:
    src: ../templates/{{ node_environment }}.env
    dest: /etc/default/edge
  notify:
  - Restart systemd service

- name: Install systemd service
  become: yes
  template:
    src: ../templates/edge.service
    dest: /etc/systemd/system/edge.service
  notify:
  - Restart systemd service

- name: Find other versions
  become: yes
  become_user: "{{ pop_user }}"
  find:
    file_type: directory
    path: "{{ pop_home }}"
    pattern: node-v*-edge-v*
    exclude: "node-v{{ node_version }}-edge-v{{ edge_version }}"
  register: expired

- name: Clean up other versions
  become: yes
  become_user: "{{ pop_user }}"
  file:
    path: "{{ item.path }}"
    state: absent
  with_items: "{{ expired.files }}"
