- name: Create restricted user for Edge
  become: yes
  user:
    name: "{{ pop_user }}"
    shell: /bin/bash

- name: Create admin user for Ansible
  become: yes
  user:
    name: "{{ pop_admin }}"
    shell: /bin/bash
    group: sudo

- name: Allow sudo for admin user without a password
  become: yes
  lineinfile:
    dest: /etc/sudoers
    state: present
    regexp: "^{{ pop_admin }}"
    line: "{{ pop_admin }} ALL=(ALL) NOPASSWD: ALL"
    validate: "visudo -cf %s"

- name: Concatenate SSH key files
  local_action:
    module: assemble
    src: keys/ssh/
    dest: /tmp/ssh_keys

- name: Install SSH keys
  become: yes
  authorized_key:
    user: "{{ pop_admin }}"
    state: present
    key: "{{ lookup('file', '/tmp/ssh_keys') }}"
    exclusive: yes

- become: yes
  block:
  - name: Secure SSH
    include_role:
      name: vitalk.secure-ssh

- name: Check if root is locked
  become: yes
  shell: grep root /etc/shadow | cut -c1-6
  register: root_is_locked
  changed_when: false

- name: Disable local root access
  when: root_is_locked.stdout != 'root:!'
  command: passwd -l root
  become: yes

- name: Remove deprecated OpenSSH options
  become: yes
  lineinfile:
    path: /etc/ssh/sshd_config
    regexp: "{{ item }}"
    state: absent
  loop:
  - Privilege Separation is turned on for security
  - UsePrivilegeSeparation
  - Lifetime and size of ephemeral version 1 server key
  - KeyRegenerationInterval
  - ServerKeyBits
  - RSAAuthentication
  - For this to work you will also need host keys in /etc/ssh_known_hosts
  - RhostsRSAAuthentication
  - IgnoreUserKnownHosts
  notify:
  - restart sshd

- name: sshd port
  become: yes
  lineinfile:
    path: /etc/ssh/sshd_config
    regexp: "^(\\s*#\\s*)?Port "
    line: "Port {{ pop_sshd_port | default('22') }}"
  notify:
  - restart sshd

- name: Disable password login
  become: yes
  lineinfile:
    path: /etc/ssh/sshd_config
    regexp: "^(\\s*#\\s*)?PasswordAuthentication "
    line: "PasswordAuthentication no"
  notify: restart sshd

- name: Allow specific SSH user
  become: yes
  lineinfile:
    path: /etc/ssh/sshd_config
    regexp: "^(\\s*#\\s*)?AllowUsers "
    line: "AllowUsers {{ pop_admin }}"
  notify: restart sshd

- name: Restrict all SSH groups
  become: yes
  lineinfile:
    path: /etc/ssh/sshd_config
    regexp: "^(\\s*#\\s*)?AllowGroups "
    state: absent
  notify: restart sshd

- name: Disable recovery boot mode on Grub Legacy
  become: yes
  lineinfile:
    create: no
    state: present
    path: /boot/grub/menu.lst
    regexp: "^# alternative="
    line: "# alternative=false"
  notify: update grub legacy
  register: result
  failed_when: result.msg != "line replaced" and result.msg and "does not exist" not in result.msg

- name: Disable recovery boot mode on Grub2
  become: yes
  lineinfile:
    create: no
    state: present
    path: /etc/default/grub
    regexp: "^(\\s*#\\s*)?GRUB_DISABLE_RECOVERY="
    line: "GRUB_DISABLE_RECOVERY=\"true\""
  notify: update grub2
  register: result
  failed_when: result.msg != "line replaced" and result.msg and "does not exist" not in result.msg

- name: Nuke rescue and emergency modes
  become: yes
  copy:
    src: "../files/{{ item }}"
    dest: "/etc/systemd/system/"
    mode: 0644
  with_items:
  - emergency.service
  - rescue.service
  notify:
  - systemd
